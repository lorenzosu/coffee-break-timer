/* -----   Globals ---------- */
let RUN = false;
let PAUSE = false;
const BUTTON_BOTTOM = '5%';
let CUSTOM_IMAGE = new Image();

/* Date constant to use date/time functions */
const DATE = new Date();

/*
Relative: initial, default behaviour: user sets minutes and seconds for
countdown.
Absolute: user selects a (clock) time and then we calculate the 
remaining minutes and seconds to that time and use those as countdown
*/
let TIME_TYPE_OPTIONS = ['Relative', 'Absolute'];
let TIME_TYPE = 0;

let USE_CUSTOM_IMAGE = false;

let LAST_THUMB_SELECTED = 0;
 
const BACKGROUNDS = [
    {
        name: "Coffee",
        url: "https://images.unsplash.com/photo-1497515114629-f71d768fd07c",
        credit: "Photo by <a href='https://unsplash.com/@asthetik?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Mike Kenneally</a>"
    },
    {
        name: "Stand-Up",
        url: "https://images.unsplash.com/photo-1611224885990-ab7363d1f2a9",
        credit: "Photo by <a href='https://unsplash.com/@edenconstantin0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Eden Constantino</a>"
    }, 
    {
        name: "Lunch",
        url: "https://images.unsplash.com/photo-1608492024938-bbdd385d5175",
        credit: "Photo by <a href='https://unsplash.com/@micheile?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Micheile Henderson</a>"
    },
    {
        name: "Starting in...",
        url: "https://images.unsplash.com/photo-1608496601160-f86d19a44f9f",
        credit: 'Photo by <a href="https://unsplash.com/@clemensvanlay?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Clemens van Lay</a>'
    },
    {
        name: "Break",
        url: "https://images.unsplash.com/photo-1592422006049-d863b5166e38",
        credit: 'Photo by <a href="https://unsplash.com/@timmossholder?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Tim Mossholder</a>'
    }
];

/*----     SET-UP   ---- */

/* Set-up the dropdown for relative (default) vs. absolute time */
let TIMETYPE_DROPDOWN = document.getElementById("time_type_dropdown");
let time_option;
for (let i = 0; i < TIME_TYPE_OPTIONS.length; i++) {
    time_option = document.createElement('option');
    time_option.text = TIME_TYPE_OPTIONS[i];
    time_option.value = i;
    TIMETYPE_DROPDOWN.add(time_option);
}

/* Set-up the dropdown for the background images */
let IMAGE_DROPDOWN = document.getElementById("background_dropdown");
let bg_option;
for (let i = 0; i < BACKGROUNDS.length; i++) {
    bg_option = document.createElement('option');
    bg_option.text = BACKGROUNDS[i].name;
    bg_option.value = i;
    IMAGE_DROPDOWN.add(bg_option);
}
bg_option = document.createElement('option');
bg_option.text = 'Custom Image';
bg_option.value = -1;
IMAGE_DROPDOWN.add(bg_option);
IMAGE_DROPDOWN.selectedIndex = 0;

/* Set the distance from bottom taking the variable in BUTTON_BOTTOM */
document.getElementById('button_div').style.bottom = BUTTON_BOTTOM;

let TITLE_INPUT = document.getElementById('title_text');
TITLE_INPUT.value = BACKGROUNDS[IMAGE_DROPDOWN.selectedIndex].name;

/* Create the first image preview */
setPreview();

/* List of Event listeners and call-backs */
const EVENTS = [
    ['time_type_dropdown', 'change', changeTimeType],
    ['background_dropdown', 'change', setPreview],
    ['button_start_stop', 'click', toggleTimer],
    ['button_pause', 'click', pauseTimer],

];
for (let ev of EVENTS) {
    let this_id = ev[0];
    let this_event = ev[1];
    let this_callback = ev[2];
    document.getElementById(this_id).addEventListener(this_event, this_callback);
}
/* Set-up handler for loading custom image */
let FILE_INPUT = document.getElementById('image_input');
FILE_INPUT.addEventListener('change', handleFiles, false);

/* END SETUP */

function changeTimeType() {
    /**
    * Handles user changing time type, reltive (i.e. total number of minute
    * and seconds) or absolut (i.e. an hour used to calcuulate the countdown)
    */
    let drop = document.getElementById("time_type_dropdown");
    let idx = drop.selectedIndex;
    let value = drop.value;
    TIME_TYPE = value
    console.log(
        `Changing time type to: ${TIME_TYPE_OPTIONS[TIME_TYPE]} [${TIME_TYPE}]`
        );
    let time1_label = document.getElementById('time1_label');
    let time1_num = document.getElementById('time1');
    let time2_label = document.getElementById('time2_label');
    let time2_num = document.getElementById('time2');
    
    // Absolute time
    if(TIME_TYPE == 1) {
        time1_label.innerHTML = "&nbsp&nbsp&nbspHour:";
        time2_label.innerHTML = "&nbspMinute:";
        let h = DATE.getHours();
        let m = DATE.getMinutes();
        let s = DATE.getSeconds();

        time1_num.min = h;
        time1_num.max = 23;

        let propose_min = m + 15;
        let propose_hour = h;
        // wrap around proposed hour:minutes using the modulo if needed
        if(propose_min > 59) {
            propose_min = propose_min % 60;
            propose_hour = propose_hour + 1;
        }
        time1_num.value = propose_hour;
        time2_num.value = propose_min;
        
    // Relative time
    } else {
        time1_label.innerHTML = "minutes";
        time2_label.innerHTML = "seconds";
        
        time1_num.min = 0;
        time1_num.max = 59;
        time1_num.value = 15;
        time2_num.value = 0;
    }
}

function handleFiles(e) {
    /**
     * Select a custom image file (locally) for the background
     * Credit: This was useful: https://jsfiddle.net/metal3d/4m3r3/
     */
    let URL = window.webkitURL || window.URL;
    let url = URL.createObjectURL(e.target.files[0]);
    CUSTOM_IMAGE.src = url;
    USE_CUSTOM_IMAGE = true;
    document.getElementById('theme_thumb').src = CUSTOM_IMAGE.src;
    let image_drowpdown = document.getElementById("background_dropdown");
    image_drowpdown.selectedIndex = 3;
    TITLE_INPUT.value = "Back in...";

}

function setPreview() {
    /**
     * Set the image preview src for the bacground image
     */
    let drop = document.getElementById("background_dropdown");
    let idx = drop.selectedIndex;
    let value = drop.value;
    // This in case the user cancels when selecting from the dropdown
    if (value === '-1') {
        CUSTOM_IMAGE = new Image();
        FILE_INPUT.click();
        if (CUSTOM_IMAGE.src != '') {
            LAST_THUMB_SELECTED = idx;
            return true;
        }
        else {
            console.log("cancelled?");
            drop.selectedIndex = LAST_THUMB_SELECTED;
            idx = drop.selectedIndex;
        }
    }
    // This based on the unsplash API for setting a 120px heigth thumb
    // See: https://unsplash.com/developers
    let img_url = BACKGROUNDS[idx].url + "?fit=clip&w=200&h=120";
    document.getElementById("theme_thumb").src = img_url;
    CUSTOM_IMAGE.src = "";
    TITLE_INPUT.value = BACKGROUNDS[IMAGE_DROPDOWN.selectedIndex].name;

    USE_CUSTOM_IMAGE = false;
    LAST_THUMB_SELECTED = idx;
}

function calculateAbsoluteTime() {
    /**
     * Calculate total seconds to absolute time (in future) specified by the
     * user. We assume absolute time has been selected!
     */
    let time1_num = document.getElementById('time1');
    let time2_num = document.getElementById('time2');
    let future = new Date();
    future.setHours(time1_num.value);
    future.setMinutes(time2_num.value);
    future.setSeconds(0);
    let seconds_diff = Math.round((future - DATE) * 0.001);
    
    return seconds_diff;
}

function validateTimeInput() {
    /**
     * Time input has min and max, but user could still write arbitrary input
     * like a string or a number that doesnt make sense... we validate this for
     * the two cases of relative and absolute time.
     * We return an object with isValid bool and a possible error message if not
    */
    let valid = {
        'isValid': true,
        'error_message': ""
    };
    // absolute time    
    if(TIME_TYPE == 1) {
        let min_hour = DATE.getHours();
        let hour = parseInt(document.getElementById("time1").value);
        let minute = parseInt(document.getElementById("time2").value);
        let validNumbers = (
            (hour != NaN) && (hour >= min_hour) &&
            (minute != NaN) && (minute >= 0) && (minute <= 59)
        );
        if (validNumbers != true)
        {
            valid.isValid = false;
            valid.error_message = `Please input a valid number greater than ${min_hour} for hours.
A number value between 0 and 59 for minutes.
You input:
${hour} for hour
${minute} for minutess`;
        }
    // relative time
    } else {
        let min = parseInt(document.getElementById("time1").value);
        let sec = parseInt(document.getElementById("time2").value);
        let validNumbers = (
            (min != NaN) && (min >= 0) && 
            (sec != NaN) && (sec >= 0) && (sec <= 59)
        );
    
        if (validNumbers != true)
        {
            valid.isValid = false;
            valid.error_message = `Please input a valid number greater than 0 for minutes.
A number value between 0 and 59 for seconds.
You input:
${min} minutes
${sec} seconds`;
        }
    }

    return valid;
}

function startAll() {
    /**
     * Set-up everything for starting the timer and eventually start it by
     * calling the startTimer funcion
     */
    let tot_sec = 0;
    let validNumbers = validateTimeInput();
    if(validNumbers['isValid'] == false) {
        alert(validNumbers['error_message']);
        return;
    }

    if(TIME_TYPE == 1) {
        tot_sec = calculateAbsoluteTime();
    } else {
        let min = parseInt(document.getElementById("time1").value);
        let sec = parseInt(document.getElementById("time2").value);  
        tot_sec = (min * 60) + sec;
    }

    document.getElementById("button_start_stop").innerHTML = "Finish";

    document.getElementById("form_div").style.visibility = "hidden";
    document.getElementById("button_pause").style.display = "";
    document.getElementById("button_div").style.bottom = "1%";
    console.log("Custom img url: " + CUSTOM_IMAGE.src);

    // Set-up the background image depending if custom or 'stock'
    if (USE_CUSTOM_IMAGE == false) {
        let idx = document.getElementById("background_dropdown").selectedIndex;
        let img_url = BACKGROUNDS[idx].url + "?auto=format?auto=compress";
        document.getElementById("background").style.backgroundImage = `url("${img_url}")`;
        console.log("setting url for background: " + img_url);
        document.getElementById("credit").innerHTML = BACKGROUNDS[idx].credit;
        let links = document.getElementById('credit').getElementsByTagName('a');
        for (let i = 0; i < links.length; i++) {
            links[i].setAttribute('target', '_blank');
        }
    } else {
        document.getElementById("background").style.backgroundImage = `url("${CUSTOM_IMAGE.src}")`;
        document.getElementById("credit").innerHTML = '';
    }

    // Change the page title
    document.getElementById("title").innerHTML = TITLE_INPUT.value;
    let minutes = Math.floor(tot_sec / 60) + "";
    let seconds = tot_sec % 60 + "";

    // Output the result in an element with id="timer"
    document.getElementById("timer").innerHTML = minutes.padStart(2, "0") + ":" + seconds.padStart(2, "0");
    document.getElementById("start_button_tip").setAttribute('data-tip', 'Stop and Reset');

    startTimer(tot_sec);
}

function stopAll() {
    /**
     * Stop everything and return to initial screen.
     * Hide / show elements accordingly.
     */

    document.getElementById("button_start_stop").innerHTML = "Start";

    document.getElementById("form_div").style.visibility = "visible";
    document.getElementById("button_pause").style.display = "none";
    
    document.getElementById("button_div").style.bottom = BUTTON_BOTTOM;
    document.getElementById("button_pause").classList.remove("glow_effect");

    document.getElementById("button_pause").innerHTML = 'Pause';

    document.getElementById("start_button_tip").setAttribute('data-tip', 'Start the timer');

    // Global variables saying if the timer is running or if it is paused
    RUN = false;
    PAUSE = false;
}


function toggleTimer() {
    /**
     * This is the function which toggles everything.
     * This is triggered by the Start/Stop button
     */
    if (RUN == false) {
        startAll();
    } else {
        stopAll();
    }
}

function pauseTimer() {
    /**
     * Pause or un-pause the timer (with the "Pause" button
     */ 
    if (PAUSE == false) {
        document.getElementById("button_pause").innerHTML = "Resume";
        document.getElementById("timer").innerHTML = "[" + document.getElementById("timer").innerHTML + "]";
        document.getElementById("button_pause").classList.add("glow_effect");
        
        PAUSE = true;
    } else {
        document.getElementById("button_pause").innerHTML = "Pause";
        document.getElementById("button_pause").classList.remove("glow_effect");
        
        PAUSE = false;
    }
}

function startTimer(total) {
    /**
     * This starts the actual timer including timer logic
     */
    let total_seconds = total;
    RUN = true;
    /* The timer works by calling a setInterval function with an interval of
       1000 milliseconds (i.e. 1 second)
       */
    let x = setInterval(function() {
        // If we are paused, simply return. Maybe this could be done better.
        if (PAUSE == true) {
            return;
        }
        // At each interval decrease the total_seconds counter by 1
        total_seconds -= 1;

        let minutes = Math.floor(total_seconds / 60) + "";
        let seconds = total_seconds % 60 + "";

        // Output the result in an element with id="demo"
        let display_str = minutes.padStart(2, "0") + ":" + seconds.padStart(2, "0");
        document.getElementById("timer").innerHTML = display_str;

        // If the count down is over stop the interval and write placeholder text 
        if (total_seconds <= 0 || RUN == false) {
            clearInterval(x);
            document.getElementById("timer").innerHTML = "--:--";
        }
        // note this 1000 below is the actual interval of 1000 milliseconds!
    }, 1000);
}