# Coffee Break Timer

A web based timer (countdown). Useful for screen sharing during calls etc.

Made with HTML, CSS and JavaScript

Absolute time (i.e. setting a target time H:M) is not 100% precise yet, but should work well in most cases like meetings ore webinars.

Try it here: https://lorenzosu.gitlab.io/coffee-break-timer/

**does not** work on phones nicely - yet
